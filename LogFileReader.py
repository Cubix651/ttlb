class LogFileReader:
    
    def __init__(self, path):
        self._file = open(path, 'r')
        next(self._file)
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        return self._file.__exit__(exc_type, exc_value, traceback)

    def __iter__(self):
        return self

    def __next__(self):
        return next(self._file)