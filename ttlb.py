#!/usr/bin/env python3
import sys
from LogFileReader import LogFileReader
from LogProcessor import LogProcessor
from DistributionPlotGenerator import DistributionPlotGenerator
from RequestStatistics import RequestStatistics

def main():
    if len(sys.argv) != 3:
        print('Provide path to log file and to output file')
        return
    
    pathToLogFile = sys.argv[1]
    pathToOutputFile = sys.argv[2]
    reader = LogFileReader(pathToLogFile)
    plot_generator = DistributionPlotGenerator()
    processor = LogProcessor(reader, plot_generator, RequestStatistics)

    processor.process()

    plot_generator.generate_plot(pathToOutputFile)

if __name__ == '__main__':
    main()