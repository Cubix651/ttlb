import matplotlib.pyplot as plt
import matplotlib.mlab as mlab

class DistributionPlotGenerator:

    def __init__(self):
        self._categories_count = 5
        self._samples = [dict() for _ in range(self._categories_count)]
        self._total_samples_count = 0

    def add_sample(self, size, ttlb):
        self._total_samples_count += 1
        category = self._category(size)
        group = self._samples[category]
        if ttlb in group:
            group[ttlb] += 1
        else:
            group[ttlb] = 1
    
    def _category(self, size):
        if size < 100*1024:
            return 0
        if size < 1024*1024:
            return 1
        if size < 10*1024*1024:
            return 2
        if size < 100*1024*1024:
            return 3
        return 4
    
    def _category_name(self, number):
        return ['<100K', '>100K<1M', '>1M<10M', '>10M<100M', '>100M<1G'][number]
        
        
    def generate_plot(self, path):        
        fig, ax = plt.subplots()

        bar_width = 0.1

        plt.xlabel('TTLB in ms')
        plt.ylabel('% of TTLB times in sample')
        plt.title('Time To Last Byte Distribution Per Object Size Class')
        
        plt.xscale('log', basex=2)
        plt.xticks([2**x for x in range(0, 30, 5)])

        for index, group in enumerate(self._samples):
            x = []
            y = []
            for ttlb, count in group.items():
                x.append(ttlb)
                y.append(100.0*count/self._total_samples_count)
            plt.bar(x, y, bar_width, label=self._category_name(index))

        plt.legend()
        #plt.tight_layout()
        #plt.show()
        plt.savefig(path)