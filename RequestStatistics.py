class RequestStatistics:

    def __init__(self, size, request_time, transfer_time, turnaround_time):
        self.size = size
        self.request_time = request_time
        self.transfer_time = transfer_time
        self.turnaround_time = turnaround_time
    
    @staticmethod
    def from_log_line(line):
        http_status,size,request_time,transfer_time,ts,turnaround_time = line.strip().split(',')
        return RequestStatistics(int(size), int(request_time), int(transfer_time), int(turnaround_time))
    
    def get_ttlb(self):
        return self.request_time + self.transfer_time + self.turnaround_time