#!/usr/bin/env python3
import unittest
from tests.RequestStatisticsTest import *
from tests.LogFileReaderTest import *
from tests.LogProcessorTest import *

def main():
    unittest.main()

if __name__ == '__main__':
    main()