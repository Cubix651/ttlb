class LogProcessor:
    
    def __init__(self, reader, plot_generator, statistics_cls):
        self._reader = reader
        self._plot_generator = plot_generator
        self._statistics_cls = statistics_cls
    
    def process(self):
        with self._reader as reader:
            for line in reader:
                stats = self._statistics_cls.from_log_line(line)
                ttlb = stats.get_ttlb()
                self._plot_generator.add_sample(stats.size, ttlb)