import unittest
from unittest import mock
from LogProcessor import LogProcessor

class InMemoryLogReader:
    def __init__(self, logs):
        self._logs = logs
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_value, traceback):
        return False

    def __iter__(self):
        return iter(self._logs)


class SimpleStatistics:
    def __init__(self, size, ttlb):
        self.size = size
        self.ttlb = ttlb

    @staticmethod
    def from_log_line(line):
        size, ttlb = map(int, line.split(','))
        return SimpleStatistics(size, ttlb)
    
    def get_ttlb(self):
        return self.ttlb


class LogProcessorTest(unittest.TestCase):
    def test_process(self):
        reader = InMemoryLogReader(['1,2','3,4'])
        plot_generator = mock.MagicMock()
        processor = LogProcessor(reader, plot_generator, SimpleStatistics)

        processor.process()

        calls = [mock.call(1,2), mock.call(3,4)]
        plot_generator.add_sample.assert_has_calls(calls, any_order=True)