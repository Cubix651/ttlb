import unittest
from RequestStatistics import RequestStatistics

class RequestStatisticsTest(unittest.TestCase):

    def test_from_log_line(self):
        line = '_,1,2,3,_,4\n'

        stats = RequestStatistics.from_log_line(line)

        self.assertEqual(stats.size, 1)
        self.assertEqual(stats.request_time, 2)
        self.assertEqual(stats.transfer_time, 3)
        self.assertEqual(stats.turnaround_time, 4)
    
    def test_get_ttlb(self):
        stats = RequestStatistics(1,2,4,8)

        ttlb = stats.get_ttlb()

        self.assertEqual(ttlb, 14)