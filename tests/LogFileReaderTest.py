import unittest
from LogFileReader import LogFileReader

class LogFileReaderTest(unittest.TestCase):

    def test_read(self):
        path = 'tests/test_log.csv'
        lines = []
        
        with LogFileReader(path) as reader:
            for line in reader:
                lines.append(line)

        self.assertEqual(lines, ['206,589926,0,768,1452106800.003,1'])